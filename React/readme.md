# 介绍和安装
React JS是Facebook开发的专门针对view层的框架，React本身 **Just fro View**, 通过构建组件实现复杂UI，有一定的学习成本，适合大规模多人协作的项目。

## React VS VUE VS Angular
目前主流的三大前端框架。
* Angular本身实现完整的MVC模型，React&VUE都是只针对View的，需要其它框架配合实现MVC或其它模型。
* React和VUE均采用虚拟Dom来实现整个view的渲染和修改，相比Angular的直接修改Dom的方式更高效，这也是React和VUE最核心的特点和功能。
* VUE沿用了html，React采用JSX。
* 三大框架均可以通过定义组件的方式构建复杂页面。

## React 特点
* 组件化，代码易与复用和修改。
* 单向数据流
* 虚拟DOM
* 生态完善，配合插件丰富。

## 前置基础
* ES6
* node & npm
* webpack

## 安装
http://www.runoob.com/react/react-install.html

**通过create-react-app脚手架安装**
此脚手架搭建的项目基于React+Webpack
```
npm install -g create-react-app
create-react-app my-first-react
cd my-first-react
npm start
```

## my-first-react 说明
* [readme](./my-first-react/readme.md)




